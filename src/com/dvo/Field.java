package com.dvo;

import java.util.Random;

public class Field {
    private final int[][] FIELD = new int[3][3];
    private final char[] PATTERN = {'☐', '☒', '⌼'};
    private boolean firstMove = true;
    private boolean gameOver = false;
    private int winner = 0;
    private int turn = 0;
    private boolean isFinish = false;
    Field() {
        drawField();
    }
    public int getWinner(){
        return winner;
    }

    public synchronized boolean isGameOver() {
        if (FIELD[0][0] == FIELD[0][1] && FIELD[0][1] == FIELD[0][2] && FIELD[0][0] != 0) {
            gameOver = true;
            winner = FIELD[0][0];
        } else if (FIELD[1][0] == FIELD[1][1] && FIELD[1][1] == FIELD[1][2] && FIELD[1][0] != 0) {
            gameOver = true;
            winner = FIELD[1][0];
        } else if (FIELD[2][0] == FIELD[2][1] && FIELD[2][1] == FIELD[2][2] && FIELD[2][0] != 0) {
            gameOver = true;
            winner = FIELD[2][0];
        } else if (FIELD[0][0] == FIELD[1][0] && FIELD[1][0] == FIELD[2][0] && FIELD[0][0] != 0) {
            gameOver = true;
            winner = FIELD[0][0];
        } else if (FIELD[0][1] == FIELD[1][1] && FIELD[1][1] == FIELD[2][1] && FIELD[0][1] != 0) {
            gameOver = true;
            winner = FIELD[0][1];
        } else if (FIELD[0][2] == FIELD[1][2] && FIELD[1][2] == FIELD[2][2] && FIELD[0][2] != 0) {
            gameOver = true;
            winner = FIELD[0][2];
        } else if (FIELD[0][0] == FIELD[1][1] && FIELD[1][1] == FIELD[2][2] && FIELD[0][0] != 0) {
            gameOver = true;
            winner = FIELD[0][0];
        } else if (FIELD[0][2] == FIELD[1][1] && FIELD[1][1] == FIELD[2][0] && FIELD[0][2] != 0) {
            gameOver = true;
            winner = FIELD[0][2];
        }
        if ((getFreeCellsCount() == 0)&&(winner == 0)) {
            gameOver = true;
            winner = 0;
        }
        if((gameOver)&&(!isFinish)) {
            isFinish = true;
            switch (winner) {
                case 0:
                    System.out.println("Game over! Draw!");
                    break;
                case 1:
                    System.out.println("Game over! \"X\"-Gamer is a winner!");
                    break;
                default:
                    System.out.println("Game over! \"O\"-Gamer is a winner!");
            }
        }
        this.notify();
        return gameOver;
    }

    public int getFreeCellsCount(){
        int result;
        result = 0;
        for (int i = 0; i <3 ; i++) {
            for (int j = 0; j < 3; j++) {
                if(FIELD[i][j] == 0){
                    result++;
                }
            }
        }
        return result;
    }

    public void setSign(int x, int y, int sign) throws InvalidTurnException{

        if(firstMove){
            turn = sign;
            Random random = new Random();
            x = random.nextInt(3);
            y = random.nextInt(3);
            firstMove = false;
        }
        if(turn == sign){
            turn = (turn == 2) ?  1: 2;
        }else{
            throw new InvalidTurnException();
        }
        FIELD[x][y] = sign;
        drawField();

         this.notify();
    }

    public int getSign(int x, int y) {
        return FIELD[x][y];
    }

    private void drawField() {
        for (int[] raw : FIELD) {
            for (int cell : raw) {
                System.out.print(PATTERN[cell] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
}
