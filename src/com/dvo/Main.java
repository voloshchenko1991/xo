package com.dvo;

public class Main {

    public static void main(String[] args) {
        Field field = new Field();
        Gamer gamer1 = new Gamer('X', field);
        Gamer gamer2 = new Gamer('O', field);
        Thread gamerThread1 = new Thread(gamer1);
        Thread gamerThread2 = new Thread(gamer2);
        gamerThread1.start();
        gamerThread2.start();
    }
}
