package com.dvo;



public class Gamer implements Runnable {
    private final int SIGN;
    private final int OPPONENT_SIGN;
    private final Field field;

    Gamer(char side, Field inputField) {
        if (side == 'x' || side == 'X') {
            SIGN = 1;
            OPPONENT_SIGN = 2;
        } else if (side == 'o' || side == 'O') {
            SIGN = 2;
            OPPONENT_SIGN = 1;
        } else {
            SIGN = 0;
            OPPONENT_SIGN = 0;
        }
        field = inputField;
    }

    private void move() {
        int x = 0;
        int y = 0;
        synchronized (field) {
            x = 4;
            y = 4;
            stop:
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    if(field.getSign(i,j) == 0){
                        if(field.getSign(checkCoordinates(i-1),checkCoordinates(j-1)) == OPPONENT_SIGN
                                && field.getSign(checkCoordinates(i+1),checkCoordinates(j+1)) == OPPONENT_SIGN &&
                                i == 1 && j == 1){
                                x = i;
                                y = j;
                                break stop;
                        }
                        if(field.getSign(checkCoordinates(i-1),j) == OPPONENT_SIGN
                                && field.getSign(checkCoordinates(i+1),j) == OPPONENT_SIGN){
                            x = i;
                            y = j;
                            break stop;
                        }
                        if(field.getSign(checkCoordinates(i-1),checkCoordinates(j+1)) == OPPONENT_SIGN
                                && field.getSign(checkCoordinates(i+1),checkCoordinates(j-1)) == OPPONENT_SIGN &&
                                i == 1 && j == 1){
                            x = i;
                            y = j;
                            break stop;
                        }
                        if(field.getSign(i,checkCoordinates(j-1)) == OPPONENT_SIGN
                                && field.getSign(i,checkCoordinates(j+1)) == OPPONENT_SIGN){
                            x = i;
                            y = j;
                            break stop;
                        }
                    }
                }
            }
            if(x == 4){
                exit:
                for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {
                        if(field.getSign(i,j) == 0){
                            x = i;
                            y = j;
                            break exit;
                        }
                    }
                }
            }
            field.setSign(x, y, SIGN);
            try {
                field.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private int checkCoordinates(int inputCoordinate) {
        return (inputCoordinate < 0) ? 2 : (inputCoordinate > 2) ? 0 : inputCoordinate;
    }


    @Override
    public void run() {
        int winner;
        char xo;
        do {
            xo = (SIGN == 1) ? 'X' : 'O';
            System.out.println("\"" + xo + "\"" + " made the step");
            move();
        } while (!field.isGameOver());
        System.out.println("\"" + xo + "\"" + " has left the game");
    }
}
